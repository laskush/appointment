package com.laskush.appointment.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.laskush.appointment.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;

public class SelectCategoryActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    //views
    SearchableSpinner selectDoctorType;
    LinearLayout doctorLayout, patientLayout;
    Button next;


    //variable
    String category = "not selected";
    String specialist;
    ArrayList<String> categoryList = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_slide5);

        doctorLayout = (LinearLayout) findViewById(R.id.doctorLayout);
        patientLayout = (LinearLayout) findViewById(R.id.patientLayout);
        selectDoctorType = (SearchableSpinner) findViewById(R.id.selectDoctorType);
        selectDoctorType.setVisibility(View.GONE);
        next = (Button) findViewById(R.id.next);

        selectDoctorType.setOnItemSelectedListener(this);

        doctorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                category = "doctor";
                selectDoctorType.setVisibility(View.VISIBLE);
                setupDoctorCategory();

            }
        });

        patientLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDoctorType.setVisibility(View.GONE);
                category = "patient";
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (category.equalsIgnoreCase("not selected")) {
                    Toast.makeText(SelectCategoryActivity.this, "Select who are you !!!", Toast.LENGTH_SHORT).show();
                } else {
                    if (category.equalsIgnoreCase("doctor")) {
                        if (specialist.equalsIgnoreCase("")) {
                            Toast.makeText(SelectCategoryActivity.this, "Select Your Speciality", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SelectCategoryActivity.this, "You continued as a "  + category, Toast.LENGTH_SHORT).show();
                            Intent logininIntent = new Intent(SelectCategoryActivity.this,LoginActivity.class);
                            startActivity(logininIntent);

                        }

                    }else{
                        Toast.makeText(SelectCategoryActivity.this, "You continued as a "  + category, Toast.LENGTH_SHORT).show();
                        Intent logininIntent = new Intent(SelectCategoryActivity.this,LoginActivity.class);
                        startActivity(logininIntent);
                    }
                }
            }
        });
    }

    private void setupDoctorCategory() {
        categoryList = new ArrayList<>();
        categoryList.add("Select Your Category");
        categoryList.add("Cardiology");
        categoryList.add("Surgeon");
        categoryList.add("Dentist");
        categoryList.add("General Physician");
        categoryList.add("Gyanecology");
        categoryList.add("Neurology");
        categoryList.add("Pathology");
        categoryList.add("Radiology");
        categoryList.add("Gastroenterology");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SelectCategoryActivity.this, android.R.layout.simple_spinner_item, categoryList);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        selectDoctorType.setAdapter(dataAdapter);


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        if (adapterView.getId() == R.id.selectDoctorType) {
            if (i > 0) {
                specialist = categoryList.get(i);
            } else {
                specialist = "";
                //showCustomViewSnackbar(getResources().getString(R.string.error_district));
            }

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
