package com.laskush.appointment.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.laskush.appointment.R;

public class DoctorDetailActivity extends AppointmentDetailActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_detail);
    }
}
