package com.laskush.appointment.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.laskush.appointment.R;

/**
 * Created by Rajan Shrestha on 9/15/2017.
 */

public class LoginActivity extends AppCompatActivity {

    TextView skip;
    EditText editTextPassword;
    CheckBox checkbox;
    TextView textViewSignUp;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        skip = (TextView) findViewById(R.id.skip);

        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        checkbox = (CheckBox) findViewById(R.id.checkbox);
        textViewSignUp = (TextView) findViewById(R.id.textViewSignUp);
        String styledText = "Don't have an account? <font color='#38AEE4'>SIGN UP</font>.";
        textViewSignUp.setText(Html.fromHtml(styledText));


        textViewSignUp.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signupIntent = new Intent(LoginActivity.this,SignupActivity.class);
                startActivity(signupIntent);
            }
        });

        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                if(value){
                    editTextPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else{
                    editTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(i);
                finish();
            }
        });


    }
}
