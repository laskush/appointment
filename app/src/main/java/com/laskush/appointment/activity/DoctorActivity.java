package com.laskush.appointment.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.laskush.appointment.R;
import com.laskush.appointment.adapter.DoctorAdapter;
import com.laskush.appointment.model.DoctorDTO;

import java.util.ArrayList;

public class DoctorActivity extends AppCompatActivity {

    //viewa
    RecyclerView recyclerViewDocotor;

    //variables


    //instances
    DoctorAdapter adapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);

        recyclerViewDocotor = (RecyclerView) findViewById(R.id.recyclerViewDoctor);

        getDoctorList();
    }

    private void getDoctorList() {

        ArrayList<DoctorDTO> doctorList = new ArrayList<>();
        DoctorDTO doctorDTO = new DoctorDTO();

        doctorList.add(new DoctorDTO(R.drawable.docotor1,"Dr. Rajan Shrestha","MBBS, DNB - Cardiologist",4,"Sahid Ganga Lal National Heart Center"));
        doctorList.add(new DoctorDTO(R.drawable.docotor1,"Dr. Rajan Shrestha","MBBS, DNB - Cardiologist",4,"Sahid Ganga Lal National Heart Center"));
        doctorList.add(new DoctorDTO(R.drawable.docotor1,"Dr. Rajan Shrestha","MBBS, DNB - Cardiologist",4,"Sahid Ganga Lal National Heart Center"));
        doctorList.add(new DoctorDTO(R.drawable.docotor1,"Dr. Rajan Shrestha","MBBS, DNB - Cardiologist",4,"Sahid Ganga Lal National Heart Center"));
        doctorList.add(new DoctorDTO(R.drawable.docotor1,"Dr. Rajan Shrestha","MBBS, DNB - Cardiologist",4,"Sahid Ganga Lal National Heart Center"));
        doctorList.add(new DoctorDTO(R.drawable.docotor1,"Dr. Rajan Shrestha","MBBS, DNB - Cardiologist",4,"Sahid Ganga Lal National Heart Center"));
        doctorList.add(new DoctorDTO(R.drawable.docotor1,"Dr. Rajan Shrestha","MBBS, DNB - Cardiologist",4,"Sahid Ganga Lal National Heart Center"));
        doctorList.add(new DoctorDTO(R.drawable.docotor1,"Dr. Rajan Shrestha","MBBS, DNB - Cardiologist",4,"Sahid Ganga Lal National Heart Center"));
        doctorList.add(new DoctorDTO(R.drawable.docotor1,"Dr. Rajan Shrestha","MBBS, DNB - Cardiologist",4,"Sahid Ganga Lal National Heart Center"));
        doctorList.add(new DoctorDTO(R.drawable.docotor1,"Dr. Rajan Shrestha","MBBS, DNB - Cardiologist",4,"Sahid Ganga Lal National Heart Center"));
        doctorList.add(new DoctorDTO(R.drawable.docotor1,"Dr. Rajan Shrestha","MBBS, DNB - Cardiologist",4,"Sahid Ganga Lal National Heart Center"));

        setupDoctorList(doctorList);
    }

    private void setupDoctorList(ArrayList<DoctorDTO> doctorList) {

        adapter = new DoctorAdapter(doctorList, DoctorActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplication());
        recyclerViewDocotor.setLayoutManager(mLayoutManager);
        recyclerViewDocotor.setItemAnimator(new DefaultItemAnimator());
        recyclerViewDocotor.setAdapter(adapter);
    }
}
