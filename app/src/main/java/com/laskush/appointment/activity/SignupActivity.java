package com.laskush.appointment.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.laskush.appointment.R;

public class SignupActivity extends AppCompatActivity {

    TextView textViewSignIn;
    TextView editTextPassword,editTextConfirmPassword;
    CheckBox checkbox;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        checkbox = (CheckBox) findViewById(R.id.checkbox);
        editTextPassword = (TextView) findViewById(R.id.editTextPassword);
        editTextConfirmPassword = (TextView) findViewById(R.id.editTextConfirmPassword);
        textViewSignIn = (TextView) findViewById(R.id.textViewSignIn);
        String styledText = "Have an account? <font color='#38AEE4'>SIGN IN</font>.";
        textViewSignIn.setText(Html.fromHtml(styledText));


        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                if(value){
                    editTextPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    editTextConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else{
                    editTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    editTextConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        textViewSignIn.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signinIntent = new Intent(SignupActivity.this,LoginActivity.class);
                startActivity(signinIntent);
                finish();
            }
        });
    }
}
