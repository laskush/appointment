package com.laskush.appointment.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.laskush.appointment.R;
import com.laskush.appointment.Utility.PrefUtils;
import com.laskush.appointment.Utility.Typewriter;

/**
 * Created by Rajan Shrestha on 9/14/2017.
 */

public class SplashActivity extends Activity {

    private static String TAG = SplashActivity.class.getSimpleName();
    private static int SPLASH_TIME =5000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final Typewriter tw = (Typewriter) findViewById(R.id.typewriter_tv);
        tw.setText("");
        tw.setCharacterDelay(80);
        tw.animateText("Book doctor appointment with a few clicks         ");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                // Checking for first time launch
                PrefUtils prefUtils= new PrefUtils(SplashActivity.this);
                if (prefUtils.isFirstTimeLaunch()) {

                    Intent i = new Intent(SplashActivity.this,WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }else{

                    Intent i = new Intent(SplashActivity.this,LoginActivity.class);
                    startActivity(i);
                    finish();
                }

                /*//Checking For Login

                if(prefUtils.isUserLoggedIn(SplashActivity.this)){

                }else{
                    *//* progressBar.setVisibility(View.GONE);
                Intent i = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(i);*//*
                }*/


            }
        }, SPLASH_TIME);
    }
}
