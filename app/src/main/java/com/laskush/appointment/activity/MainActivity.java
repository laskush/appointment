package com.laskush.appointment.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.laskush.appointment.R;
import com.laskush.appointment.Utility.BottomNavigationViewEx;
import com.laskush.appointment.Utility.BottomNavigationViewHelper;
import com.laskush.appointment.fragment.AppointmentFragment;
import com.laskush.appointment.fragment.HomeFragment;
import com.laskush.appointment.fragment.ProfileFragment;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    BottomNavigationViewEx navigation;

    private BottomNavigationViewEx.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationViewEx.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.home_menu:
                    Toast.makeText(MainActivity.this, "Home", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.appointment_menu:
                    Toast.makeText(MainActivity.this, "Appointment", Toast.LENGTH_SHORT).show();
                    return true;

                case R.id.profile_menu:
                    Toast.makeText(MainActivity.this, "Profile", Toast.LENGTH_SHORT).show();
                    return true;

                case R.id.reminder_menu:
                    Toast.makeText(MainActivity.this, "Reminder", Toast.LENGTH_SHORT).show();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupBottomNavigationView();
    }


    /**
     * BottomNavigationView setup
     */
    private void setupBottomNavigationView() {
        Log.d(TAG, "setupBottomNavigationView: setting up BottomNavigationView");
        navigation = (BottomNavigationViewEx) findViewById(R.id.navigation);
        BottomNavigationViewHelper.setupBottomNavigationView(navigation);

        enableNavigation(MainActivity.this, navigation);
        Menu menu = navigation.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        loadFragment(new HomeFragment());
    }

    public void enableNavigation(final Context context, BottomNavigationViewEx view) {
        view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.home_menu:
                        loadFragment(new HomeFragment());
                        return true;
                    case R.id.appointment_menu:
                        loadFragment(new AppointmentFragment());
                        return true;

                    case R.id.profile_menu:
                        loadFragment(new ProfileFragment());
                        return true;

                    case R.id.reminder_menu:
                        Toast.makeText(MainActivity.this, "Reminder", Toast.LENGTH_SHORT).show();
                        return true;
                }


                return false;
            }
        });
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        //transaction.addToBackStack(null);
        transaction.commit();
    }

}
