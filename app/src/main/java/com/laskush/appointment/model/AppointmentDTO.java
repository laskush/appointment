package com.laskush.appointment.model;

public class AppointmentDTO {

    private String status;
    private String title;
    private String description;
    private String group;
    private String time;

    public AppointmentDTO() {
    }

    public AppointmentDTO(String status, String title, String description, String group, String time) {
        this.status = status;
        this.title = title;
        this.description = description;
        this.group = group;
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
