package com.laskush.appointment.model;

public class DoctorDTO {

    private int icon;
    private String name;
    private String shortDetail;
    private float rating;
    private String hospitalName;

    public DoctorDTO() {
    }

    public DoctorDTO(int icon, String name, String shortDetail, float rating, String hospitalName) {
        this.icon = icon;
        this.name = name;
        this.shortDetail = shortDetail;
        this.rating = rating;
        this.hospitalName = hospitalName;
    }


    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDetail() {
        return shortDetail;
    }

    public void setShortDetail(String shortDetail) {
        this.shortDetail = shortDetail;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }
}
