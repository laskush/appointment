package com.laskush.appointment.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.laskush.appointment.R;
import com.laskush.appointment.adapter.SpecialistAdapter;
import com.laskush.appointment.model.SpecialistDTO;

import java.util.ArrayList;


public class HomeFragment extends Fragment {

    //viewa
    RecyclerView recyclerView_Specialist;

    //variables


    //instances
    SpecialistAdapter adapter;

    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        recyclerView_Specialist = (RecyclerView) view.findViewById(R.id.recyclerView_Specialist);

        getSpecialist();
    }

    private void getSpecialist() {

        ArrayList<SpecialistDTO> specialistList = new ArrayList<>();
        SpecialistDTO specialistDTO = new SpecialistDTO();

        specialistList.add(new SpecialistDTO("Dentist", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("Dernatology", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("Endocrinology", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("Gastroenterology", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("General Physician", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("Surgeons", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("Cardiologists", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("Anesthesiology", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("Immunologists", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("Neurology", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("Pathology", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("Radiology", R.drawable.dentist));
        specialistList.add(new SpecialistDTO("Gyanecology", R.drawable.dentist));

        setupSpecialist(specialistList);
    }

    private void setupSpecialist(ArrayList<SpecialistDTO> specialistList) {

        adapter = new SpecialistAdapter(specialistList, getContext());
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView_Specialist.setLayoutManager(mLayoutManager);
        recyclerView_Specialist.setItemAnimator(new DefaultItemAnimator());
        recyclerView_Specialist.setAdapter(adapter);
    }

}
