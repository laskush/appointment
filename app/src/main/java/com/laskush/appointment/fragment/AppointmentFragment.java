package com.laskush.appointment.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.laskush.appointment.R;
import com.laskush.appointment.adapter.AppointmentAdapter;
import com.laskush.appointment.adapter.SpecialistAdapter;
import com.laskush.appointment.model.AppointmentDTO;
import com.laskush.appointment.model.SpecialistDTO;

import java.util.ArrayList;

public class AppointmentFragment extends Fragment {

    //viewa
    RecyclerView recyclerView_Appointment;

    //variables


    //instances
    AppointmentAdapter adapter;

    public AppointmentFragment() {
        // Required empty public constructor
    }


    public static AppointmentFragment newInstance(String param1, String param2) {
        AppointmentFragment fragment = new AppointmentFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_appointment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        recyclerView_Appointment = (RecyclerView) view.findViewById(R.id.recyclerView_Appointment);

        getSpecialist();
    }

    private void getSpecialist() {

        ArrayList<AppointmentDTO> appointmentList = new ArrayList<>();
        AppointmentDTO appointmentDTO = new AppointmentDTO();

        appointmentList.add(new AppointmentDTO("Pending","Toothache - Pain or swelling in my mouth", "Toothaches usually indicate a cavity but they can also signal gum disease. In some cases, a toothache is a sign of an abscess or impacted tooth. A toothache should be evaluated by a dentist right away to determine the cause of the problem and prevent the tooth from dying. ","Dentist","8th june 2019"));
        appointmentList.add(new AppointmentDTO("Later","Toothache - Pain or swelling in my mouth", "Toothaches usually indicate a cavity but they can also signal gum disease. In some cases, a toothache is a sign of an abscess or impacted tooth. A toothache should be evaluated by a dentist right away to determine the cause of the problem and prevent the tooth from dying. ","Dentist","8th june 2019"));
        appointmentList.add(new AppointmentDTO("Reject","Toothache - Pain or swelling in my mouth", "Toothaches usually indicate a cavity but they can also signal gum disease. In some cases, a toothache is a sign of an abscess or impacted tooth. A toothache should be evaluated by a dentist right away to determine the cause of the problem and prevent the tooth from dying. ","Dentist","8th june 2019"));
        appointmentList.add(new AppointmentDTO("Accept","Toothache - Pain or swelling in my mouth", "Toothaches usually indicate a cavity but they can also signal gum disease. In some cases, a toothache is a sign of an abscess or impacted tooth. A toothache should be evaluated by a dentist right away to determine the cause of the problem and prevent the tooth from dying. ","Dentist","8th june 2019"));
        appointmentList.add(new AppointmentDTO("Reject","Toothache - Pain or swelling in my mouth", "Toothaches usually indicate a cavity but they can also signal gum disease. In some cases, a toothache is a sign of an abscess or impacted tooth. A toothache should be evaluated by a dentist right away to determine the cause of the problem and prevent the tooth from dying. ","Dentist","8th june 2019"));
        appointmentList.add(new AppointmentDTO("Accept","Toothache - Pain or swelling in my mouth", "Toothaches usually indicate a cavity but they can also signal gum disease. In some cases, a toothache is a sign of an abscess or impacted tooth. A toothache should be evaluated by a dentist right away to determine the cause of the problem and prevent the tooth from dying. ","Dentist","8th june 2019"));
        appointmentList.add(new AppointmentDTO("Pending","Toothache - Pain or swelling in my mouth", "Toothaches usually indicate a cavity but they can also signal gum disease. In some cases, a toothache is a sign of an abscess or impacted tooth. A toothache should be evaluated by a dentist right away to determine the cause of the problem and prevent the tooth from dying. Toothaches usually indicate a cavity but they can also signal gum disease. In some cases, a toothache is a sign of an abscess or impacted tooth. A toothache should be evaluated by a dentist right away to determine the cause of the problem and prevent the tooth from dying. ","Dentist","8th june 2019"));
        appointmentList.add(new AppointmentDTO("Later","Toothache - Pain or swelling in my mouth", "Toothaches usually indicate a cavity but they can also signal gum disease. In some cases, a toothache is a sign of an abscess or impacted tooth. A toothache should be evaluated by a dentist right away to determine the cause of the problem and prevent the tooth from dying. ","Dentist","8th june 2019"));
        appointmentList.add(new AppointmentDTO("Accept","Toothache - Pain or swelling in my mouth", "Toothaches usually indicate a cavity but they can also signal gum disease. In some cases, a toothache is a sign of an abscess or impacted tooth. A toothache should be evaluated by a dentist right away to determine the cause of the problem and prevent the tooth from dying. ","Dentist","8th june 2019"));

        setupAppointment(appointmentList);
    }

    private void setupAppointment(ArrayList<AppointmentDTO> appointmentList) {

        Log.d("Fragment App : ", "" + appointmentList.size());
        adapter = new AppointmentAdapter(appointmentList, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_Appointment.setLayoutManager(mLayoutManager);
        recyclerView_Appointment.setItemAnimator(new DefaultItemAnimator());
        recyclerView_Appointment.setAdapter(adapter);
    }
}
