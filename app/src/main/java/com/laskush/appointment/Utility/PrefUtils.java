package com.laskush.appointment.Utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Rajan Shrestha on 10/13/2017.
 */

public class PrefUtils {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    public static final String SHARED_PREF_DR_APPOINTMENT= "dr_appointment";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public static final String EMAIL_KEY_IS_LOGGED_IN = "is_emaillogged_in";

    public PrefUtils(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(SHARED_PREF_DR_APPOINTMENT, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }


    public  boolean isUserLoggedIn(Context context) {

        return pref.getBoolean(EMAIL_KEY_IS_LOGGED_IN, false);
    }


    public  void setUserLoggedIn(Context context, boolean isLogin) {

        editor.putBoolean(EMAIL_KEY_IS_LOGGED_IN, isLogin);
        editor.commit();
    }
}
