package com.laskush.appointment.Utility;

import android.util.Log;


import com.laskush.appointment.Utility.BottomNavigationViewEx;
public class BottomNavigationViewHelper {

    private static final String TAG = BottomNavigationViewHelper.class.getName();

    public static void setupBottomNavigationView(BottomNavigationViewEx bottomNavigationViewEx){
        Log.d(TAG, "setupBottomNavigationView: Setting up BottomNavigationView");
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.setTextVisibility(true);
    }
}