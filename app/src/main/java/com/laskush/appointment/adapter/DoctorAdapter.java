package com.laskush.appointment.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.laskush.appointment.R;
import com.laskush.appointment.Utility.CircleImageView;
import com.laskush.appointment.activity.DoctorDetailActivity;
import com.laskush.appointment.model.DoctorDTO;
import com.laskush.appointment.model.SpecialistDTO;

import java.util.List;

public class DoctorAdapter extends RecyclerView.Adapter<DoctorAdapter.MyViewHolder>{

    private List<DoctorDTO> doctorDTOList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView_doctor;
        CircleImageView doctor_image;
        TextView doctor_name,doctor_shortDetail,doctor_hospitalName;
        RatingBar ratingBar;

        public MyViewHolder(View view) {
            super(view);

            cardView_doctor = (CardView) view.findViewById(R.id.cardView_doctor);
            doctor_image = (CircleImageView) view.findViewById(R.id.doctor_image);
            doctor_name = (TextView) view.findViewById(R.id.doctor_name);
            doctor_shortDetail = (TextView) view.findViewById(R.id.doctor_shortDetail);
            doctor_hospitalName = (TextView) view.findViewById(R.id.doctor_hospitalName);
            ratingBar = (RatingBar) view.findViewById(R.id.doctor_ratingBar);

            cardView_doctor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent doctorDetailIntent = new Intent(context, DoctorDetailActivity.class);
                    context.startActivity(doctorDetailIntent);
                }
            });

        }
    }


    public DoctorAdapter(List<DoctorDTO> doctorList,Context context) {
        this.doctorDTOList = doctorList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_doctor, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DoctorDTO doctorDTO = doctorDTOList.get(position);

        holder.doctor_image.setImageResource(doctorDTO.getIcon());
        holder.doctor_name.setText(doctorDTO.getName());
        holder.doctor_shortDetail.setText(doctorDTO.getShortDetail());
        holder.doctor_hospitalName.setText(doctorDTO.getHospitalName());
        holder.ratingBar.setRating(doctorDTO.getRating());
    }

    @Override
    public int getItemCount() {
        return doctorDTOList.size();
    }
}

