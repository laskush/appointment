package com.laskush.appointment.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laskush.appointment.R;
import com.laskush.appointment.activity.DoctorActivity;
import com.laskush.appointment.model.SpecialistDTO;

import java.util.List;

public class SpecialistAdapter extends RecyclerView.Adapter<SpecialistAdapter.MyViewHolder>{

    private List<SpecialistDTO> specialistDTOList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layout_specialist;
        TextView specialistName;
        ImageView specialistImage;

        public MyViewHolder(View view) {
            super(view);

            layout_specialist = (LinearLayout) view.findViewById(R.id.layout_specialist);
            specialistName = (TextView) view.findViewById(R.id.specialist_name);
            specialistImage = (ImageView) view.findViewById(R.id.specialist_icon);

            layout_specialist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent doctorIntent = new Intent(context, DoctorActivity.class);
                    context.startActivity(doctorIntent);
                }
            });

        }
    }


    public SpecialistAdapter(List<SpecialistDTO> specialistList,Context context) {
        this.specialistDTOList = specialistList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_specialist, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SpecialistDTO specialistDTO = specialistDTOList.get(position);

        holder.specialistName.setText(specialistDTO.getName());
        //holder.specialistImage.setImageDrawable(context.getResources().getDrawable(specialistDTO.getIcon()));
        holder.specialistImage.setImageResource(specialistDTO.getIcon());
    }

    @Override
    public int getItemCount() {
        return specialistDTOList.size();
    }
}
