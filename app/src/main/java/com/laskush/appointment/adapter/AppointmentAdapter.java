package com.laskush.appointment.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.laskush.appointment.R;
import com.laskush.appointment.activity.AppointmentDetailActivity;
import com.laskush.appointment.model.AppointmentDTO;

import java.util.List;

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentAdapter.MyViewHolder> {

    private List<AppointmentDTO> appointmentDTOList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView status, title, description, group, date;
        CardView cardView_appointment;

        public MyViewHolder(View view) {
            super(view);

            cardView_appointment = (CardView) view.findViewById(R.id.cardView_appointment);
            status = (TextView) view.findViewById(R.id.textView_appointmentStatus);
            title = (TextView) view.findViewById(R.id.textView_appointmentTitle);
            description = (TextView) view.findViewById(R.id.textView_appointmentDesc);
            group = (TextView) view.findViewById(R.id.textView_specialistGroup);
            date = (TextView) view.findViewById(R.id.textView_appointmentSendDate);

            cardView_appointment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent appDetailIntent = new Intent(context, AppointmentDetailActivity.class);
                    context.startActivity(appDetailIntent);
                }
            });

        }
    }


    public AppointmentAdapter(List<AppointmentDTO> appointmentList, Context context) {
        this.appointmentDTOList = appointmentList;
        this.context = context;
        Log.d("Fragment Adapter : ", "" + appointmentList.size());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_appointment, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AppointmentDTO appointmentDTO = appointmentDTOList.get(position);

        Log.d("Fragment Adapter : ", "" + appointmentDTO.getGroup());

        String status = appointmentDTO.getStatus();
        if (status.equalsIgnoreCase("pending")) {
            holder.status.setText("Pending");
            holder.status.setBackground(context.getResources().getDrawable(R.drawable.orange_circle));
        } else if (status.equalsIgnoreCase("reject")) {
            holder.status.setText("Reject");
            holder.status.setBackground(context.getResources().getDrawable(R.drawable.red_circle));
        } else if (status.equalsIgnoreCase("later")) {
            holder.status.setText("Later");
            holder.status.setBackground(context.getResources().getDrawable(R.drawable.later_circle));
        } else if (status.equalsIgnoreCase("accept")) {
            holder.status.setText("Accept");
            holder.status.setBackground(context.getResources().getDrawable(R.drawable.green_circle));
        }

        holder.title.setText(appointmentDTO.getTitle());
        holder.description.setText(appointmentDTO.getDescription());
        holder.group.setText(appointmentDTO.getGroup());
        holder.date.setText(appointmentDTO.getTime());
    }

    @Override
    public int getItemCount() {
        return appointmentDTOList.size();
    }
}
